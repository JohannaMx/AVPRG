## Animal Singdom - Die Tönenden Tiere

Webapp, die unter Verwendung einer Webcam Tiere erkennen kann und deren Laute abspielt. Die MIDI-Soundausgabe kann mit diversen Effekten angepasst werden. Ein Beat kann erstellt und geloopt werden, der durch die entsprechenden Tierlaute angereichert spaßige "Musik" erzeugt.

Ein Projekt von Johanna Meyer und Bennedict Schweimer. Entstanden als Prüfungsleistung des Moduls "Audio-Video-Programmierung" an der HAW-Hamburg.

## Ablaufdiagramm Beat zum Loop hinzufügen

![Beat Ablaufdiagramm](AnimalSingdom/Images/AnimalFluss.PNG)

## Ablaufdiagramm Tier hinzufügen

![Tier Ablaufdiagramm](AnimalSingdom/Images/AnimalFluss2.PNG)
